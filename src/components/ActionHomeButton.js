import React, { Component } from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";

import Icon from "react-native-vector-icons/Ionicons";
import ActionButton from "react-native-action-button";

class ActionHomeButton extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* Rest of the app comes ABOVE the action button component !*/}
        <ActionButton buttonColor="#f96b02">
          <ActionButton.Item
            buttonColor="#9b59b6"
            title="Rendez-vous"
            onPress={() => {}}
          >
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#000"
            title="Mes documents"
            onPress={() => {}}
          >
            <Icon name="md-folder" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#1abc9c"
            title="Traitement"
            onPress={() => {}}
          >
            <Icon name="md-medkit" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});

export default ActionHomeButton;
