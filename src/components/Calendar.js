import React, { Component } from "react";
import { AppRegistry, View } from "react-native";

import CalendarStrip from "react-native-calendar-strip";

export default class Calendar extends Component {
  render() {
    return (
      <View>
        <CalendarStrip
          calendarAnimation={{ type: "sequence", duration: 30 }}
          // daySelectionAnimation={{type: 'border', duration: 300, borderWidth: 1, borderHighlightColor: 'white'}}
          style={{ height: 100, paddingTop: 20, paddingBottom: 10 }}
          calendarHeaderStyle={{ color: "black" }}
          calendarColor={"#ededed"}
          dateNumberStyle={{ color: "black" }}
          dateNameStyle={{ color: "black" }}
          highlightDateNameStyle={{ color: "#f96b02" }}
          highlightDateNumberStyle={{ color: "#f96b02" }}
          iconLeft={require("../images/left-arrow.png")}
          iconRight={require("../images/right-arrow.png")}
          iconContainer={{ flex: 0.09 }}
        />
      </View>
    );
  }
}
