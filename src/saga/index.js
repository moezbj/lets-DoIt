import { fork } from "redux-saga/effects";

import { createRdvSaga } from "./RDVCreateSaga";
import { getRendezVousSaga } from "./rendezVousSaga";
import { loginSaga } from "./authSaga";

export default function*() {
  yield fork(createRdvSaga);
  yield fork(getRendezVousSaga);
  yield fork(loginSaga);
}
