import { put, call, takeEvery } from "redux-saga/effects";
import * as firebase from "firebase";

// getUsers Actions and Types
import getRendezVousActions, { rdvTypes } from "../redux/RendezVousRedux";

let data = [];

async function getData() {
  const firebaseRef = firebase.database().ref("/RDV");
  var ref = [];
  await firebaseRef.once("value", snap => {
    if (snap.val() === null) return;
    ref = Object.entries(snap.val());
    const array = ref
      .map((el, i, arr) => {
        if (typeof el === "object") {
          return { ...el[1], id: el[0] };
        }
      })
      .filter(el => el);
    data = JSON.parse(JSON.stringify(array));
  });

  return data;
}

function* rdvRequest() {
  try {
    // start request
    data = yield call(getData);
    // check failure
    if (data == null) {
      // failure action
      yield put(getRendezVousActions.rdvError("error"));
    } else {
      // success action
      yield put(getRendezVousActions.rdvSuccess(data));
    }
  } catch (error) {
    // failure action
    yield put(getRendezVousActions.rdvError(error));
  }
}

export function* getRendezVousSaga() {
  // Hookup Redux To Saga
  yield takeEvery(rdvTypes.RDV_REQUEST, rdvRequest);
}
