import { put, call, takeEvery, all } from "redux-saga/effects";
import * as firebase from "firebase";

// create Actions and Types
import createRdvActions, { createRdvTypes } from "../redux/RDVCreationRedux";
import modalActions from "../redux/modalRedux";
import getRendezVousActions from "../redux/RendezVousRedux";

// create customer function
function* createRDVRequest({ payload }) {
  try {
    // check failure
    if (payload == null) {
      // failure action
      yield put(createRdvActions.createRDVError(payload));
    } else {
      // success action
      yield all([
        yield put(createRdvActions.createRDVSuccess(payload)),
        //yield put(customersActions.customersRequest())
        yield put(modalActions.modalClose()),
        yield put(getRendezVousActions.rdvRequest())
      ]);
      firebase
        .database()
        .ref("RDV")
        .push(payload);
    }
  } catch (error) {
    // failure action
    yield put(createRdvActions.createRDVError(error));
  }
}

export function* createRdvSaga() {
  // Hookup Redux To Saga
  yield takeEvery(createRdvTypes.CREATE_R_D_V_REQUEST, createRDVRequest);
}
