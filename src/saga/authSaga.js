import { put, takeEvery, all } from "redux-saga/effects";

// create Actions and Types
import loginActions, { loginTypes } from "../redux/authRedux";

// create customer function
function* loginRequest({ payload }) {
  try {
    // check failure
    if (payload == null) {
      // failure action
      yield put(loginActions.loginError(payload));
    } else {
      // success action
      yield all([yield put(loginActions.loginSuccess(payload))]);
    }
  } catch (error) {
    // failure action
    yield put(loginActions.loginError(error));
  }
}

// logout function
function* logout() {
  this.props.navigation.navigate("login");
}

export function* loginSaga() {
  // Hookup Redux To Saga
  yield takeEvery(loginTypes.LOGIN_REQUEST, loginRequest);
  yield takeEvery(loginTypes.LOGOUT, logout);
}
