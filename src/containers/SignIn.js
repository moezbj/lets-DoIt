import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  AsyncStorage
} from "react-native";
import { StackNavigator } from "react-navigation";
import * as firebase from "firebase";
import { Dropdown } from "react-native-material-dropdown";

export default class SignIn extends Component {
  state = {
    nom: "",
    prenom: "",
    password: "",
    mail: "",
    tel: "",
    typeC: ""
  };

  signIn = user => {
    const { mail, password, typeC, lat, lng } = this.state;
    firebase
      .database()
      .ref("/users")
      .push(user);
    firebase
      .auth()
      .createUserWithEmailAndPassword(mail, password)
      .then(res => {
        this.props.navigation.navigate("home");
      });
  };

  onChangePicker = text => {
    this.setState({
      typeC: text
    });
  };

  render() {
    let data = [
      {
        value: "Admin"
      },
      {
        value: "Host"
      }
    ];
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView behavior="padding" style={styles.Wrapper}>
          <ScrollView>
            <View style={styles.container}>
              <Text style={styles.header}> Sign In </Text>

              <View style={styles.inputContainer}>
                <Text style={styles.label}> Nom</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Nom"
                  onChangeText={nom => this.setState({ nom })}
                  underlineColorAndroid="transparent"
                />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.label}> Prenom</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Prenom"
                  onChangeText={prenom => this.setState({ prenom })}
                  underlineColorAndroid="transparent"
                />
              </View>

              <View style={styles.inputContainer}>
                <Text style={styles.label}> mail</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="mail"
                  onChangeText={mail => this.setState({ mail })}
                  underlineColorAndroid="transparent"
                />
              </View>

              <View style={styles.inputContainer}>
                <Text style={styles.label}> password</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Password"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid="transparent"
                />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.label}> telephone</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="tel"
                  onChangeText={tel => this.setState({ tel })}
                  underlineColorAndroid="transparent"
                />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.label}> Type de Compte </Text>
                <Dropdown
                  label="Type de Compte"
                  data={data}
                  value={this.state.typeC}
                  onChangeText={this.onChangePicker}
                />
              </View>

              <TouchableOpacity
                style={styles.btn}
                onPress={() => this.signIn(this.state)}
              >
                <Text style={styles.textBtn}> Enregistrer </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = {
  Wrapper: {
    flex: 1
  },

  container: {
    flex: 1,

    backgroundColor: "#2896d3",
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 30
  },

  header: {
    fontSize: 24,
    marginBottom: 20,
    marginBottom: 20,
    color: "#fff",
    fontWeight: "bold"
  },
  inputContainer: {
    display: "flex",
    justifyContent: "center"
  },
  label: { color: "white", paddingBottom: 5 },

  textInput: {
    alignSelf: "stretch",
    padding: 7,
    marginBottom: 20,
    backgroundColor: "#fff",
    borderRadius: 5
  },

  btn: {
    alignSelf: "stretch",
    backgroundColor: "#01c853",
    padding: 15,
    alignItems: "center",
    borderRadius: 5
  },
  textBtn: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold"
  },
  google: {
    textInputContainer: {
      width: "100%",
      marginBottom: 20
    },
    description: {
      fontWeight: "bold"
    },
    predefinedPlacesDescription: {
      color: "#1faadb"
    },
    poweredContainer: {
      display: "none"
    }
  }
};
