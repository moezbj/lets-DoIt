import React from "react";
import {
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";

import Icon from "react-native-vector-icons/Ionicons";

class Drawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          icon: "md-person",
          title: "  Profile",
          url: "profile"
        },
        {
          icon: "md-list",
          title: "  Agenda",
          url: "profile"
        },
        {
          icon: "md-calendar",
          title: "  Mes Rendez-Vous",
          url: "RendezVous"
        },
        {
          icon: "md-calendar",
          title: "  Events"
        },
        {
          icon: "md-folder",
          title: "  Mes documents"
        },
        {
          icon: "ios-help-circle",
          title: "  Aide"
        },
        {
          icon: "ios-medal",
          title: "  About us"
        }
      ]
    };
  }

  logOut = () => {
    firebase
      .auth()
      .signOut()
      .then();
    // Sign-out successful.
    this.props.navigation.navigate("home");
  };

  render() {
    return (
      <View>
        {this.state.items.map((item, i) => (
          <TouchableOpacity
            key={i}
            style={{
              margin: 10,
              paddingLeft: 10,
              flexDirection: "row"
            }}
            onPress={() => this.props.navigation.navigate(item.url)}
          >
            <Icon name={item.icon} />
            <Text>{item.title}</Text>
          </TouchableOpacity>
        ))}
        {this.props.isLogged == false ? (
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("login")}
          >
            <Icon name="ios-help-circle" />
            <Text>Login</Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={this.logout}>
            <Icon name="ios-help-circle" />
            <Text>Log OUT</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  isLogged: state.login.isLogged
});
export default connect(
  mapStateToProps,
  null
)(Drawer);
const styles = StyleSheet.create({});
