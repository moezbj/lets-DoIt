import React, { Component } from "react";
import { TouchableOpacity, Alert } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";

import { StyleSheet, Text, View } from "react-native";
import Timeline from "react-native-timeline-listview";
import modalActions from "../redux/modalRedux";

import createRdvActions from "../redux/RDVCreationRedux";
import getRendezVousActions from "../redux/RendezVousRedux";

class RendezVous extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    console.ignoredYellowBox = ["Setting a timer"];
  }

  openCreateRDV = () => {
    if (this.props.state.login.isLogged === true) {
      this.props.navigation.navigate("AddRDVScreen");
    } else {
      Alert.alert("plz log");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Timeline
          style={styles.list}
          // onEventPress={this.onDeleteItem}
          data={this.props.data}
          circleSize={20}
          innerCircle={"dot"}
          circleColor="rgb(45,156,219)"
          lineColor="rgb(45,156,219)"
          timeContainerStyle={{ minWidth: 52, marginTop: 2 }}
          timeStyle={{
            textAlign: "center",
            backgroundColor: "#ff9797",
            color: "white",
            padding: 5,
            borderRadius: 13
          }}
          descriptionStyle={{ color: "gray" }}
          options={{
            style: { paddingTop: 2 }
          }}
        />
        <TouchableOpacity
          rounded
          style={styles.btn}
          onPress={this.openCreateRDV}
        >
          <Text style={styles.text}>Saisie un Rd </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    padding: 5
  },
  list: {
    flex: 1
  },
  btn: {
    backgroundColor: "#4682B4",
    marginBottom: 10,
    marginLeft: 5,
    padding: 5
  },
  modal: {
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 5,
    flex: 0,
    width: 300,
    height: 400
  },
  closeBtn: {
    backgroundColor: "#ff9797",
    justifyContent: "center",
    width: 60
  },
  picker: {
    backgroundColor: "#E5E5E5"
  },
  view: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 50
  }
});
const mapStateToProps = state => ({
  state,
  data: state.rendezVous.response
});
const mapDispatchToProps = dispatch => ({
  createRdv: payload => dispatch(createRdvActions.createRDVRequest(payload)),
  openModal: payload => dispatch(modalActions.modalOpen(payload)),
  closeModal: () => dispatch(modalActions.modalClose()),
  getRendezVous: payload => dispatch(getRendezVousActions.rdvRequest(payload))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RendezVous);
