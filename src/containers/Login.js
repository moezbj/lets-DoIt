import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";

import { StackNavigator } from "react-navigation";
import * as firebase from "firebase";
import loginRequestAction from "../redux/authRedux";

class AuthScreen extends Component {
  state = {
    username: "",
    password: ""
  };

  componentDidMount() {
    const firebaseRef = firebase.database().ref("/users");
    firebaseRef.on("value", snap => {
      if (snap.val() === null) return;
      // else {
      //   this.setState({
      //     ids: this.state.ids.concat(Object.keys(snap.val())),
      //     val: this.state.ids.concat(Object.values(snap.val())),
      //     array: this.state.ids.concat(Object.entries(snap.val()))
      //   });
      // }
    });
  }

  login = () => {
    const email = this.state.username;
    const password = this.state.password;
    const payload = {
      email: this.state.username,
      password: this.state.password
    };
    firebase.auth().signInWithEmailAndPassword(email, password);
    // .then(res => {
    //   console.log(res);
    //   result = this.state.array.find(el => email === el.mail);
    //   if (result) {
    //     this.setState({
    //       id: result[0]
    //     });
    //   }
    //   const itemId = res.email;
    this.props.requestLogin(payload);
    this.props.navigation.navigate("home");

    // })
    // .catch(err => {
    //   this.setState({
    //     error: true
    //   });
    // });
    _storeData = async () => {
      try {
        await AsyncStorage.setItem("isLogged", this.props.isLogged);
      } catch (error) {
        // Error saving data
      }
    };
  };
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.Wrapper}>
        <View style={styles.container}>
          <Text style={styles.header}> LOGIN </Text>
          {this.state.error && <Text style={styles.textError}>Incorrect</Text>}
          <TextInput
            style={styles.textInput}
            placeholder="username"
            onChangeText={username => this.setState({ username, error: false })}
            underlineColorAndroid="transparent"
          />

          <TextInput
            style={styles.textInput}
            placeholder="password"
            onChangeText={password => this.setState({ password, error: false })}
            underlineColorAndroid="transparent"
          />

          <TouchableOpacity style={styles.btn} onPress={this.login}>
            <Text style={styles.textBtn}> login </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("signIn")}
          >
            <Text style={styles.text}> créé une compte </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  Wrapper: {
    flex: 1
  },

  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2896d3",
    paddingLeft: 40,
    paddingRight: 40
  },

  header: {
    fontSize: 24,
    marginBottom: 60,
    color: "#fff",
    fontWeight: "bold"
  },

  textInput: {
    alignSelf: "stretch",
    padding: 16,
    marginBottom: 20,
    backgroundColor: "#fff",
    borderRadius: 5
  },

  btn: {
    alignSelf: "stretch",
    backgroundColor: "#01c853",
    padding: 15,
    alignItems: "center",
    borderRadius: 5
  },
  text: {
    alignSelf: "stretch",
    color: "#fff",
    padding: 20,
    fontSize: 18,
    alignItems: "center"
  },
  textBtn: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold"
  },
  textError: {
    color: "red",
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 15
  }
};
const mapStateToProps = state => ({
  state,
  data: state.rendezVous.response,
  isLogged: state.login.isLogged
});
const mapDispatchToProps = dispatch => ({
  requestLogin: payload => dispatch(loginRequestAction.loginRequest(payload))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthScreen);
