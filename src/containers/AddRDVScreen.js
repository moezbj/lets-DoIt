import React, { Component } from "react";
import { TouchableOpacity, TextInput } from "react-native";
import { connect } from "react-redux";

import { StyleSheet, Text, View } from "react-native";

import DatePicker from "react-native-datepicker";

import createRdvActions from "../redux/RDVCreationRedux";
import MapView from "react-native-maps";

class AddRDVScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      inputDate: "",
      inputEvent: "",
      inputDes: "",
      data: [],
      date: new Date().toISOString().split("T")[0],
      time: "",
      lat: "",
      lng: "",
      place_id: ""
    };
  }
  componentWillMount() {
    navigator.geolocation.getCurrentPosition(position => {
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      });
    });
  }
  onDateChange = newdate => {
    this.setState({
      date: newdate
    });
  };
  onChangeEvent = typedText => {
    this.setState({
      inputEvent: typedText
    });
  };
  onTimeChange = newtime => {
    this.setState({
      time: newtime
    });
  };
  getAddress = placeId => {
    const lat = placeId.latitude;
    const lng = placeId.longitude;
    const place_id = placeId.address;
    this.setState({
      lat,
      lng,
      place_id
    });
  };
  onSubmitdate = () => {
    var newAppointment = {
      time: this.state.date,
      title: this.state.inputEvent,
      description: this.state.time
    };
    this.props.createRdv(newAppointment);
  };
  AnnulerCreation = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.textContainer}>
          <Text>Nom de l'évènement</Text>
        </View>
        <View style={styles.compContainer}>
          <TextInput
            style={{ height: 50, width: 200, marginTop: 30 }}
            onChangeText={this.onChangeEvent}
            value={this.state.inputEvent}
            placeholder="Event"
          />
        </View>
        <View style={styles.textContainer}>
          <Text>Date de l'évènemet</Text>
        </View>
        <View style={styles.compContainer}>
          <DatePicker
            style={{ width: 200, marginTop: 30 }}
            date={this.state.date}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={this.onDateChange}
            customStyles={{
              dateIcon: {
                position: "absolute",
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
            }}
          />
        </View>

        <View style={styles.textContainer}>
          <Text>L'heure de évènement</Text>
        </View>
        <View style={styles.compContainer}>
          <DatePicker
            style={{ width: 200, marginTop: 30 }}
            date={this.state.time}
            mode="time"
            format="HH:mm"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            minuteInterval={10}
            onDateChange={this.onTimeChange}
          />
        </View>
        <View style={styles.textContainer}>
          <Text>L'addresse</Text>
        </View>

        <View style={styles.viewButtons}>
          <TouchableOpacity
            style={styles.closeBtn}
            onPress={this.AnnulerCreation}
          >
            <Text>Annuler</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.closeBtn} onPress={this.onSubmitdate}>
            <Text>Add</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    padding: 5
  },
  textContainer: {
    width: "100%",
    height: 20,
    alignItems: "center",
    backgroundColor: "#f96b02"
  },
  compContainer: {
    width: "100%",
    height: 100,
    alignItems: "center",
    justifyContent: "center"
  },
  list: {
    flex: 1
  },
  btn: {
    backgroundColor: "#4682B4",
    marginBottom: 10,
    marginLeft: 5,
    padding: 5
  },
  closeBtn: {
    backgroundColor: "#ff9797",
    justifyContent: "center",
    width: 60
  },
  picker: {
    backgroundColor: "#E5E5E5"
  },
  viewButtons: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 50
  }
});
const mapStateToProps = state => ({
  state,
  data: state.rendezVous.response
});
const mapDispatchToProps = dispatch => ({
  createRdv: payload => dispatch(createRdvActions.createRDVRequest(payload)),
  openModal: payload => dispatch(modalActions.modalOpen(payload)),
  closeModal: () => dispatch(modalActions.modalClose()),
  getRendezVous: payload => dispatch(getRendezVousActions.rdvRequest(payload))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddRDVScreen);
