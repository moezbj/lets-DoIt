import React from "react";
import { StyleSheet, Text, View } from "react-native";
import ActionHomeButton from "../components/ActionHomeButton";
import Calendar from "../components/Calendar";
import { connect } from "react-redux";
import getRendezVousActions from "../redux/RendezVousRedux";
import _ from "lodash";
import Modal from "react-native-modal";

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
  }
  componentWillMount() {
    this.props.getRendezVous();
  }

  render() {
    var currentTime = new Date().toISOString().split("T")[0];
    return (
      <View style={{ flex: 1 }}>
        <Calendar />

        <View>
          <View style={styles.List}>
            <Text style={styles.text}>Mes rendez-vous</Text>
          </View>
          {!_.isEmpty(this.props.data) &&
            this.props.data
              .filter(item => {
                return item.time == currentTime;
              })
              .map((item, i) => {
                return (
                  <View key={i} style={{ backgroundColor: "#fff" }}>
                    <Text> {item.title} </Text>
                    <Text>à</Text>
                    <Text> {item.description} </Text>
                  </View>
                );
              })}
        </View>
        <View
          pointerEvents="box-none"
          style={{
            position: "absolute",
            bottom: 0,
            top: 0,
            right: 0,
            left: 0
          }}
        >
          <ActionHomeButton />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  List: {
    backgroundColor: "lightgrey",
    justifyContent: "center"
  },
  text: {
    color: "white"
  },
  item: {
    color: "black"
  }
});

const mapStateToProps = state => ({
  state,
  data: state.rendezVous.response,
  isLogged: state.login.isLogged
});
const mapDispatchToProps = dispatch => ({
  getRendezVous: payload => dispatch(getRendezVousActions.rdvRequest(payload))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);
