import { createReducer, createActions } from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rdvRequest: ["payload"],
  rdvSuccess: ["response"],
  rdvError: ["error"]
});

export const rdvTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  response: null,
  error: null
};

/* ------------- Reducers ------------- */
const rdvRequest = state => ({
  ...state,
  fetching: true
});
const rdvSuccess = (state, { response }) => ({
  ...state,
  response,
  fetching: false,
  error: null
});

const rdvError = (state, { error }) => ({
  ...state,
  error,
  fetching: false,
  response: null
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RDV_REQUEST]: rdvRequest,
  [Types.RDV_SUCCESS]: rdvSuccess,
  [Types.RDV_ERROR]: rdvError
});
