import { createReducer, createActions } from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  modalToggle: ["content"],
  modalOpen: ["content"],
  modalClose: []
});

export const modalTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  open: false,
  content: null
};

/* ------------- Reducers ------------- */

const modalToggle = (state, { content }) => ({
  ...state,
  open: !state.open,
  content
});

const modalOpen = (state, { content }) => ({ ...state, open: true, content });

const modalClose = state => ({ ...state, open: false });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MODAL_TOGGLE]: modalToggle,
  [Types.MODAL_OPEN]: modalOpen,
  [Types.MODAL_CLOSE]: modalClose
});
