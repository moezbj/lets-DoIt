import { createReducer, createActions } from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  createRDVRequest: ["payload"],
  createRDVSuccess: ["response"],
  createRDVError: ["error"]
});

export const createRdvTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  fetching: false,
  response: null,
  error: null
};

/* ------------- Reducers ------------- */
const createRDVRequest = state => ({ ...state, fetching: true });
const createRDVSuccess = (state, { response }) => ({
  ...state,
  response,
  fetching: false,
  error: null
});

const createRDVError = (state, { error }) => ({
  ...state,
  error,
  fetching: false,
  response: null
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CREATE_R_D_V_REQUEST]: createRDVRequest,
  [Types.CREATE_R_D_V_SUCCESS]: createRDVSuccess,
  [Types.CREATE_R_D_V_ERROR]: createRDVError
});
