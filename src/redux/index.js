import { combineReducers } from "redux";

// startup handler
//import { reducer as startup } from "./startupRedux";

// reducers
import { reducer as rdvCreate } from "./RDVCreationRedux";
import { reducer as modal } from "./modalRedux";
import { reducer as rendezVous } from "./RendezVousRedux";
import { reducer as login } from "./authRedux";

export default combineReducers({
  rdvCreate,
  modal,
  rendezVous,
  login
});
