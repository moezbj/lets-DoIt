import { createReducer, createActions } from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  startupStart: [],
  startupEnd: ["isLogged"]
});

export const startupTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  isLogged: false,
  startupEnd: false
};

/* ------------- Reducers ------------- */
const startupStart = state => state;

const startUpEnd = (state, { isLogged }) =>
  state.merge({ isLogged, startupEnd: true });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP_START]: startupStart,
  [Types.STARTUP_END]: startUpEnd
});
