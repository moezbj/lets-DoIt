import { createReducer, createActions } from "reduxsauce";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ["payload"],
  loginSuccess: ["response"],
  loginError: ["error"],
  logout: []
});

export const loginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  isLogged: false,
  fetching: false,
  response: null,
  error: null
};

/* ------------- Reducers ------------- */

const loginRequest = () => ({
  fetching: true
});
const loginSuccess = (state, { response }) => ({
  ...state,
  fetching: false,
  error: null,
  isLogged: !state.isLogged,
  response
});
const loginError = state => ({
  ...state,
  error,
  fetching: false,
  response: null
});
const logout = () => INITIAL_STATE;

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGIN_ERROR]: loginError,
  [Types.LOGOUT]: logout
});
