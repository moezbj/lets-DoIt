import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./src/store";
import { connect } from "react-redux";

import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  AsyncStorage
} from "react-native";
import firebase from "firebase";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import Introduction from "./src/containers/Introduction";
import Drawer from "./src/containers/Drawer";
import HomeScreen from "./src/containers/HomeScreen";
import RendezVous from "./src/containers/RendezVous";
import Login from "./src/containers/Login";
import AddRDVScreen from "./src/containers/AddRDVScreen";
import SignIn from "./src/containers/SignIn";

const firebaseConfig = {
  apiKey: "AIzaSyAn2iS6tKbsDpR2RIeKRobxJd8QZpb7ntQ",
  authDomain: "do-it-9b063.firebaseapp.com",
  databaseURL: "https://do-it-9b063.firebaseio.com",
  projectId: "do-it-9b063",
  storageBucket: "do-it-9b063.appspot.com",
  messagingSenderId: "443659499717"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
_retrieveData = async () => {
  try {
    const value = await AsyncStorage.getItem("isLogged");
    if (value !== null) {
      // We have data!!
      this.setState({
        islogged: true
      });
      console.log(value);
    }
  } catch (error) {
    // Error retrieving data
  }
};
const DrawerNav = createDrawerNavigator(
  {
    intro: { screen: Introduction },
    home: { screen: HomeScreen },
    RendezVous: { screen: RendezVous },
    login: { screen: Login },
    AddRDVScreen: { screen: AddRDVScreen },
    signIn: { screen: SignIn }
  },
  {
    contentComponent: props => <Drawer {...props} />
  }
);

const MainNavigator = createStackNavigator(
  {
    intro: { screen: DrawerNav }
  },
  {
    initialRouteName: "intro",
    //initialRouteName: this.state.islogged ? "intro" : "home",
    headerMode: "none"
  }
);

/*const MainNavigatorLogged = createStackNavigator(
  {
    home: { screen: DrawerNav }
  },
  {
    initialRouteName: "home",
    headerMode: "none"
  }
);*/

class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      islogged: false
    };
  }
  componentWillMount() {
    _retrieveData().then(res => {
      /* this.setState({
        islogged:
      })*/
      console.log(res);
    });
  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <MainNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

/*const mapStateToProps = state => ({
  isLogged: state.login.isLogged
});
export default connect(
  mapStateToProps,
  null
)(App);*/
export default App;
